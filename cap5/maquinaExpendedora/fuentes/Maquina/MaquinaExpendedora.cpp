// MaquinaExpendedora.cpp: implementation of the MaquinaExpendedora class.
//
//////////////////////////////////////////////////////////////////////

#include "../../cabeceras/Maquina/MaquinaExpendedora.h"
#include <vector>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

MaquinaExpendedora::MaquinaExpendedora()
{

}

MaquinaExpendedora::~MaquinaExpendedora()
{

}

void MaquinaExpendedora::solicitarProducto()
{
	HWCajero miCajero;
	Dinero elPrecioProducto = miCajero.solicitarProducto();
	Ingresos miDinero;
	while(miDinero.haySuficienteDinero(elPrecioProducto) == false)
		miDinero.anyadirMoneda(miCajero.recibirMoneda());

	Devoluciones elDineroEntregar(miDinero.getDinero(),elPrecioProducto);

	std::vector<Moneda> laListaMonedas;
	elDineroEntregar.calcularDevoluciones(laListaMonedas);
	miCajero.entregarVueltas(laListaMonedas);
}


#include <iostream>
int main()
{
	MaquinaExpendedora laMaquinaExpendedora;
	bool continuar = true;
	char opcion;

	while(continuar)
	{
		laMaquinaExpendedora.solicitarProducto();
		std::cout<<"Nuevo producto (s/n): ";
		std::cin>>opcion;
		continuar = (opcion == 'n') || (opcion == 'N') ? false :true;
	}

	return 0;
}

