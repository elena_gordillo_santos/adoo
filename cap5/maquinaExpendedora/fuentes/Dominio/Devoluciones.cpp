// Devoluciones.cpp: implementation of the Devoluciones class.
//
//////////////////////////////////////////////////////////////////////

#include "../../cabeceras/Dominio/Devoluciones.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Devoluciones::Devoluciones()
{

}

Devoluciones::~Devoluciones()
{

}

void Devoluciones::calcularDevoluciones(std::vector<Moneda> &laListaMonedas)
{
	if(this->elDineroAdevolver.getTipoDinero() == EURO)
	{
		while(this->elDineroAdevolver.getCantidad()>= 200)
		{
			laListaMonedas.push_back(Moneda(200));
			this->elDineroAdevolver-=200;
		}
		while(this->elDineroAdevolver.getCantidad()>= 100)
		{
			laListaMonedas.push_back(Moneda(100));
			this->elDineroAdevolver-=100;
		}
		while(this->elDineroAdevolver.getCantidad()>= 50)
		{
			laListaMonedas.push_back(Moneda(50));
			this->elDineroAdevolver-=50;
		}
		while(this->elDineroAdevolver.getCantidad()>= 20)
		{
			laListaMonedas.push_back(Moneda(20));
			this->elDineroAdevolver-=20;
		}
		while(this->elDineroAdevolver.getCantidad()>= 10)
		{
			laListaMonedas.push_back(Moneda(10));
			this->elDineroAdevolver-=10;
		}
		while(this->elDineroAdevolver.getCantidad()>= 5)
		{
			laListaMonedas.push_back(Moneda(5));
			this->elDineroAdevolver-=5;
		}
		while(this->elDineroAdevolver.getCantidad()>= 2)
		{
			laListaMonedas.push_back(Moneda(2));
			this->elDineroAdevolver-=2;
		}
		while(this->elDineroAdevolver.getCantidad()>= 1)
		{
			laListaMonedas.push_back(Moneda(1));
			this->elDineroAdevolver-=1;
		}

	}
}

