// Devoluciones.h: interface for the Devoluciones class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DEVOLUCIONES_H__59407FA2_733A_44B1_8467_D54AFF48AD90__INCLUDED_)
#define AFX_DEVOLUCIONES_H__59407FA2_733A_44B1_8467_D54AFF48AD90__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <vector>
#include "../comunes/Moneda.h"

class Devoluciones
{
	Dinero elDineroAdevolver;
public:
	Devoluciones();
	Devoluciones(Dinero elIngreso,Dinero elPedido)
	{elDineroAdevolver = elIngreso-elPedido;}
	virtual ~Devoluciones();
	void calcularDevoluciones(std::vector<Moneda> &);

};

#endif // !defined(AFX_DEVOLUCIONES_H__59407FA2_733A_44B1_8467_D54AFF48AD90__INCLUDED_)
