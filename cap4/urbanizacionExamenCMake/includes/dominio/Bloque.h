// Bloque.h: interface for the Bloque class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BLOQUE_H__136A4D4F_08DC_4523_9B6B_88516241CC4C__INCLUDED_)
#define AFX_BLOQUE_H__136A4D4F_08DC_4523_9B6B_88516241CC4C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class Bloque  
{
	float x,y,z;
	float base, altura;
public:
	Bloque();
	virtual ~Bloque();
	void setPosicion(float ax,float ay, float az)
	{x=ax; y = ay; z =az;}
	void setBase(float ancho) {base=ancho;altura=ancho;}
	float getAltura() {return altura;}
	void dibuja();
	void setAltura(float alto) {altura = alto;}

};

#endif // !defined(AFX_BLOQUE_H__136A4D4F_08DC_4523_9B6B_88516241CC4C__INCLUDED_)
