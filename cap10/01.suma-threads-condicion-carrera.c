// Compilar con opcion: -lpthread
// gcc 01.pipe....c -o suma -lpthread

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/syscall.h>

#define NUM_THREADS 10

int suma_total = 0;

void * sumar(void *n) {
	int inicio, fin, sumParcial=0, i;
	
	inicio = (long)n * 10 + 1;
	fin = inicio + 9;

	for (i=inicio; i<=fin; i++) {
		sumParcial+=i;
	}

	printf ("Thread suma parcial [%d-%d] = %d\n", inicio, fin, sumParcial);
	// LA SIGUIENTE SENTENCIA PUEDE DAR LUGAR A UNA CONDICION DE CARRERA
	suma_total += sumParcial;
	pthread_exit(0);
}

int main() {
	pthread_attr_t attr;
	pthread_t thid[NUM_THREADS];
	int i;

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	
	for(i=0; i<NUM_THREADS; i++) {	
		pthread_create(&thid[i], &attr, sumar, (void *)(long)i); // un long es del mismo tam que un puntero
	}
	
	/* Wait on the other threads */
	for(i=0;i<NUM_THREADS;i++) {
  		pthread_join(thid[i], NULL);
  	}
	
	printf("Resultado = %d\n", suma_total);

	return 0;
}
