#include <iostream>
#include <string>
#include <vector>

using namespace std;

typedef enum{PAYPAL, TARJETA} TipoPago;

class MetodoPago {
protected:
	TipoPago tipo;
	int id;
public:
	static MetodoPago * metodoFabricacion(TipoPago, string, int);
	virtual void imprimir() {
		cout << "Metodo Pago " << id;
	}
};

class Paypal : public MetodoPago{
	string correo;
	friend class MetodoPago;
	Paypal(string &c,int &i):correo(c){ id=i; }
public:
	void imprimir() {
		MetodoPago::imprimir();
		cout<< " - " << correo << endl;
	}

};

class Tarjeta : public MetodoPago{
	string numero;
	friend class MetodoPago;
	Tarjeta(string &n,int &i):numero(n){ id=i; }
public:
	void imprimir() {
		MetodoPago::imprimir();
		cout<< " - " << numero << endl;
	}

};

class Cuenta {
	vector<MetodoPago *> pagos;
	string nombre;
	string dni;
public:
	Cuenta(string n, string d):nombre(n), dni(d){}
	~Cuenta() {
		for(int i=0;i<pagos.size();i++)
			delete pagos[i];
	}

	void imprimir() {
		cout << "Cuenta: " << nombre << " - " << dni << endl;
		for(int i=0;i<pagos.size();i++)
			pagos[i]->imprimir();

	}

	void insertarPago(MetodoPago *p) {
		pagos.push_back(p);
	}

	int getNPagos() {
		return pagos.size();
	}
};

MetodoPago * MetodoPago::metodoFabricacion(TipoPago tipo, string texto, int id)
{
	if(tipo==PAYPAL) return new Paypal(texto, id);
	else if(tipo==TARJETA) return new Tarjeta(texto, id);
	else return 0;
}


int main()
{
	Cuenta usuario("Luis Lopez", "06111222L");
	usuario.insertarPago(MetodoPago::metodoFabricacion(PAYPAL,"luis.lopez@upm.es", usuario.getNPagos()+1));
	usuario.insertarPago(MetodoPago::metodoFabricacion(TARJETA,"5555000011112222", usuario.getNPagos()+1));
	usuario.imprimir();
	return 0;
}

